package es.mor.PruebaImatia;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet que gestiona las acciones b�sicas de GestorTareas.jsp
 * 
 * @author Miguel Otero
 */
public class Controlador extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Controlador() {
        super();
    }

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        
      //A�adimos datos de prueba por defecto en la carga inicial
        List<Tarea> listaTareas = new ArrayList<Tarea>();

        Tarea t1 = new Tarea("Reuni�n de Scrum","Reuni�n diaria de Scrum",0);
        Tarea t2 = new Tarea("Entrenamiento","Entrenamiento a las 20:00",0);
        Tarea t3 = new Tarea("Preparar viaje","Preparar equipaje para el viaje",1);
        Tarea t4 = new Tarea("Reservar cena","Cena a las 10:00",0);
        Tarea t5 = new Tarea("Documentaci�n veterinario","Llevar la documentaci�n solicitada",1);
        
        listaTareas.add(t1);
        listaTareas.add(t2);
        listaTareas.add(t3);
        listaTareas.add(t4);
        listaTareas.add(t5);
        
        getServletContext().setAttribute("listaTareas", listaTareas);
        getServletContext().setAttribute("tareaActiva", t1);
        config.getServletContext().setAttribute("tareaActiva", t1);
        
        ServletContext sc = getServletContext();
        sc.setAttribute("tareaActiva", t1);
      }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		@SuppressWarnings("unchecked")
		List<Tarea> listaTareas = (ArrayList<Tarea>) request.getSession(true).getAttribute("listaTareas");
		Tarea tareaActiva = (Tarea)request.getSession().getAttribute("tareaActiva");
		
		if (request.getParameter("actionAnhadir") != null) {
			//TO-DO codigo para anhadir nueva tarea
		}
		else if (request.getParameter("actionEliminar") != null) {
			//TO-DO codigo para eliminar nueva tarea
		}
		else if (request.getParameter("actionPendiente") != null) {
			//TO-DO codigo para cambiar de estado
		}
		else if (request.getParameter("actionFinalizar") != null) {
			//TO-DO codigo para cambiar de estado
		}

		getServletContext().getRequestDispatcher("/GestorTareas.jsp").forward(request, response);
        
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	
	 @SuppressWarnings({ "unchecked", "unused" })
	private void anhadirTarea(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {

	        HttpSession sesion = request.getSession();
	        ArrayList<Tarea> listaTareas;
	        
	        //Verificamos que est� cargada la lista de Tareas, si no, la registramos.
	        if (sesion.getAttribute("listaTareas") == null) {
	        	listaTareas = new ArrayList<Tarea>();
	        } else {
	        	listaTareas = (ArrayList<Tarea>) sesion.getAttribute("listaTareas");
	        }
	        
	        //Aqui crear�amos la nueva tarea
	        
	        //Actualizamos la sesion
	        sesion.setAttribute("listaTareas", listaTareas);
	        response.sendRedirect("GestorTareas.jsp");
	    }
}
	
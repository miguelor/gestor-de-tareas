package es.mor.PruebaImatia;

import java.util.Date;

/***
 * Clase que representa una Tarea con sus respectivos campos.
 * 
 * @author Miguel Otero
 *
 */
public class Tarea {

	String titulo;
	Date fechaCreacion;
	String descripcion;
	int estado;
	
	public Tarea() 
	{}
	
	public Tarea(String titulo, String descripcion,
			int estado) {
		super();
		this.titulo = titulo;
		this.descripcion = descripcion;
		this.estado = estado;
	}
	
	public Tarea(String titulo, Date fechaCreacion, String descripcion,
			int estado) {
		super();
		this.titulo = titulo;
		this.fechaCreacion = fechaCreacion;
		this.descripcion = descripcion;
		this.estado = estado;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getEstado() {
		return estado;
	}

	public String getEstadoString() {
		if(this.estado == 0)
			return "Pendiente";
		else
			return "Finalizada";
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}
	
	
}

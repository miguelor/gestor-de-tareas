<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page import="java.io.*,java.util.*"%>
<%@ page import="javax.servlet.*,java.text.*"%>
<%@ page import="es.mor.PruebaImatia.*"%>
<%@ page session="true"%>
<%

	

	///////
	Integer idTarea;

	if (request.getParameter("idTarea") != null) {
		idTarea = Integer.parseInt(request.getParameter("idTarea"));
	} else {
		idTarea = 0;
	}

	List<Tarea> listaTareas = (List<Tarea>) getServletContext().getAttribute("listaTareas");
	Tarea tareaActiva = listaTareas.get(idTarea);
%>

<html>
	<head>

	<link rel="stylesheet" type="text/css" href="estilo.css">
	<link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">

	<script>
		document.getElementById('idTarea').value = getUrlParam('idTarea', '0');

		//Refresca el id de la tarea seleccionada de la lista
		function seleccionTarea(idTarea) {
			window.location.href = "/GestorTareas/GestorTareas.jsp?idTarea="+ idTarea;
			document.getElementById('idTarea').value = idTarea;
		}


		function getUrlVars() {
			var vars = {};
			var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
					function(m, key, value) {
						vars[key] = value;
					});
			return vars;
		}
	
		function getUrlParam(parameter, defaultvalue) {
			var urlparameter = defaultvalue;
			if (window.location.href.indexOf(parameter) > -1) {
				urlparameter = getUrlVars()[parameter];
			}
			return urlparameter;
		}
	</script>
</head>
<body>
	<h1>Listado de tareas - Miguel Otero - Septiembre 2018</h1>

	<div id="contenedorPrincipal">
		<div id="listaTareas">
			<c:forEach items="${listaTareas}" var="item" varStatus="indices">
				<c:choose>
					<c:when test="${item.estado == 0}">
						<c:set var="class" scope="page" value="tareaPendiente" />
					</c:when>
					<c:when test="${item.estado == 1}">
						<c:set var="class" scope="page" value="tareaFinalizada" />
					</c:when>
					<c:otherwise>
						<c:set var="class" scope="page" value="tareaPendiente" />
					</c:otherwise>
				</c:choose>

				<ul class="${class}" id="${indices.index}"
					onclick="seleccionTarea(this.id);">${item.titulo}</ul>
			</c:forEach>
		</div>

		<form method="post" action="Controlador">
			<input type="hidden" id="idTarea" value="" /> 
			<input type="submit" value="" class="btnAnhadir" title="A�adir tarea" alt="A�adir tarea" name="actionAnhadir" /> 
			<input type="submit" value="" class="btnEliminar" title="Eliminar tarea" alt="Eliminar tarea" name="actionEliminar" /> 
			<input type="submit" value="" class="btnEstadoPendiente" title="Pasar a estado 'Pendiente'" alt="Pasar a estado 'Pendiente'" name="actionPendiente" /> 
			<input type="submit" value="" class="btnEstadoFinalizada" title="Pasar a estado 'Finalizada'" alt="Pasar a estado 'Finalizada'" name="actionFinalizar" />
		</form>
		<div id="descripcionTareas">
			<p>Tarea:</p>
			<textarea id="textAreaNombreTarea" cols="80">${tareaActiva.titulo}</textarea>
			<br/>
			<p>Informaci�n de la tarea:</p>
			<textarea id="textAreaInfoTarea" cols="80" rows="20">${tareaActiva.descripcion}</textarea>
		</div>
	</div>
</body>
</html>